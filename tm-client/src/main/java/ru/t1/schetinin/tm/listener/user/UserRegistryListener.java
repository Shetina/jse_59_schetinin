package ru.t1.schetinin.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.schetinin.tm.dto.request.UserRegistryRequest;
import ru.t1.schetinin.tm.dto.model.UserDTO;
import ru.t1.schetinin.tm.event.ConsoleEvent;
import ru.t1.schetinin.tm.util.TerminalUtil;

@Component
public final class UserRegistryListener extends AbstractUserListener {

    @NotNull
    private static final String NAME = "user-registry";

    @NotNull
    private static final String DESCRIPTION = "Registry user.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@userRegistryListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(getToken());
        request.setLogin(login);
        request.setPassword(password);
        request.setEmail(email);
        @Nullable final UserDTO user = userEndpoint.registryUser(request).getUser();
        showUser(user);
    }

}