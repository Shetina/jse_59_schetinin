package ru.t1.schetinin.tm.repository.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.schetinin.tm.api.repository.model.IRepository;
import ru.t1.schetinin.tm.comparator.CreatedComparator;
import ru.t1.schetinin.tm.comparator.StatusComparator;
import ru.t1.schetinin.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
@AllArgsConstructor
@NoArgsConstructor
public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @Getter
    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        if (comparator == StatusComparator.INSTANCE) return "status";
        else return "name";
    }

    @NotNull
    public abstract Class<M> getEntity();

    @NotNull
    @Override
    public M add(@NotNull final M model) throws Exception {
        entityManager.persist(model);
        return model;
    }

    @Override
    public void clear() throws Exception {
        @Nullable final List<M> models = findAll();
        if (models == null) return;
        for (@NotNull final M model : models) {
            remove(model);
        }
    }

    @Override
    public boolean existsById(@NotNull final String id) throws Exception {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public List<M> findAll() throws Exception {
        @NotNull final String jpql = "SELECT m FROM " + getEntity().getSimpleName() + " m";
        return entityManager.createQuery(jpql, getEntity()).getResultList();
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Comparator comparator) throws Exception {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<M> criteriaQuery = criteriaBuilder.createQuery(getEntity());
        @NotNull final Root<M> from = criteriaQuery.from(getEntity());
        criteriaQuery.select(from);
        criteriaQuery.orderBy(criteriaBuilder.asc(from.get(getSortType(comparator))));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) throws Exception {
        return entityManager.find(getEntity(), id);
    }

    @Override
    public int getSize() throws Exception {
        @NotNull final String jpql = "SELECT COUNT(m) FROM " + getEntity().getSimpleName() + " m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult().intValue();
    }

    @Override
    public void remove(@NotNull final M model) throws Exception {
        entityManager.remove(model);
    }

    @Override
    public void update(@NotNull final M model) throws Exception {
        entityManager.merge(model);
    }

}