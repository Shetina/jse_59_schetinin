package ru.t1.schetinin.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.t1.schetinin.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.schetinin.tm.enumerated.Sort;

import java.util.List;

public interface IUserOwnedDTOService<M extends AbstractUserOwnedModelDTO> extends IUserOwnedDTORepository<M> {

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Sort sort) throws Exception;

    void removeById(@Nullable String userId, @Nullable String id) throws Exception;

}
