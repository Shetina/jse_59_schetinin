package ru.t1.schetinin.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.schetinin.tm.api.repository.model.ISessionRepository;
import ru.t1.schetinin.tm.model.Session;

import javax.persistence.EntityManager;

@Repository
@Scope("prototype")
@NoArgsConstructor
public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    @NotNull
    @Override
    public Class<Session> getEntity() {
        return Session.class;
    }

}