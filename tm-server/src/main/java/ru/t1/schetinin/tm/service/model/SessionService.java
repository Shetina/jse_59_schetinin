package ru.t1.schetinin.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.schetinin.tm.api.repository.model.ISessionRepository;
import ru.t1.schetinin.tm.api.service.model.ISessionService;
import ru.t1.schetinin.tm.model.Session;

@Service
@NoArgsConstructor
public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    @NotNull
    @Autowired
    private ISessionRepository repository;

    @NotNull
    protected ISessionRepository getRepository() {
        return repository;
    }

}